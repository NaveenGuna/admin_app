import 'package:flutter/material.dart';

Color primaryColor = const Color(0xFF01579B);
Color blackColor = const Color(0xFF000000);
Color darkGreyColor = const Color(0x9E131212);