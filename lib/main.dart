import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:sendbird_sdk/sendbird_sdk.dart';
import 'package:admin_app/routers/router.dart';

final sendBird = SendbirdSdk(appId: "C5752D21-333C-4A8C-A583-AD49AD76DEE8");

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const AppInit());
}

class AppInit extends StatefulWidget {
  const AppInit({Key? key}) : super(key: key);

  @override
  State<AppInit> createState() => _AppInitState();
}

class _AppInitState extends State<AppInit> {
  // Handle the notifications received.
  /* FirebaseMessaging.onMessage.listen(
  (RemoteMessage message) {
  print('title: ${message.notification?.title}');
  print('body: ${message.notification?.body}');
  // Parse the message received.
  PushNotification notification = PushNotification(
  title: message.notification?.title,
  body: message.notification?.body,
  );

  setState(() {
  _notificationInfo = notification;
  _totalNotifications++;
  });
  if (_notificationInfo != null) {
  NotificationService.showNotification(
  _notificationInfo?.title ?? '', _notificationInfo?.body ?? '');
  }
  });*/

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: GetPageRouter.loginPageRoute,
      theme: ThemeData(primarySwatch: Colors.lightGreen),
      getPages: GetPageRouter.routes,
    );
  }

  @override
  void initState() {
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    FirebaseMessaging.onMessage.listen((event) {
      var value = event.data['sendbird'];
      var body = jsonDecode(value);
      flutterLocalNotificationsPlugin.show(
          0,
          body['message'],
          body['message'],
          NotificationDetails(
            android: AndroidNotificationDetails(
              event.messageId ?? "",
              "Testing",
              color: Colors.blue,
              playSound: true,
              icon: '@mipmap/ic_launcher',
            ),
          ));
    });
    FirebaseMessaging.onMessageOpenedApp.listen((event) {});
    FirebaseMessaging.onBackgroundMessage((message) async {
      var value = message.data['sendbird'];
      var body = jsonDecode(value);
      flutterLocalNotificationsPlugin.show(
          0,
          body['message'],
          body['message'],
          NotificationDetails(
            android: AndroidNotificationDetails(
              message.messageId ?? "",
              "Testing",
              color: Colors.blue,
              playSound: true,
              icon: '@mipmap/ic_launcher',
            ),
          ));
    });
  }
}
