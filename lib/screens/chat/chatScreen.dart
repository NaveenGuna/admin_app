import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sendbird_sdk/sendbird_sdk.dart' as sb;
import 'package:admin_app/resources/borderDesigns.dart';
import 'package:admin_app/resources/dimen.dart';
import 'package:admin_app/routers/router.dart';
import 'package:admin_app/screens/chat/ChatScreenController.dart';
import 'package:admin_app/screens/fireStore/entity/User.dart' as u;

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with BorderDesign, Dimension {
  late u.User user;
  late ChatScreenController chatCtrl;
  double borderRadius = 12;
  TextEditingController msgCtrl = TextEditingController();

  Future _loadMsgFuture = Future.value();


  @override
  void dispose() {
    chatCtrl.dispose();
    Get.delete<ChatScreenController>();
  }

  @override
  void initState() {
    // TODO: implement initState
    user = Get.arguments[0][0];
    chatCtrl = Get.put(ChatScreenController(user));
    _loadMsgFuture = chatCtrl.getMessages();
    super.initState();
  }

  void callEmoji() {
    print('Emoji Icon Pressed...');
  }

  void callAttachFile() {
    print('Attach File Icon Pressed...');
  }

  void callCamera() {
    print('Camera Icon Pressed...');
  }

  void callVoice() {
    print('Voice Icon Pressed...');
  }

  Widget moodIcon() {
    return IconButton(
        icon: const Icon(
          Icons.mood,
          color: Color(0xFF00BFA5),
        ),
        onPressed: () => callEmoji());
  }

  Widget attachFile() {
    return IconButton(
      icon: const Icon(Icons.attach_file, color: Color(0xFF00BFA5)),
      onPressed: () => callAttachFile(),
    );
  }

  Widget camera() {
    return IconButton(
      icon: const Icon(Icons.photo_camera, color: Color(0xFF00BFA5)),
      onPressed: () => callCamera(),
    );
  }

  Widget voiceIcon() {
    return const Icon(
      Icons.keyboard_voice,
      color: Colors.white,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: InkWell(
              onTap: () {
                Get.toNamed(GetPageRouter.userProfilePageRoute, arguments: [
                  [chatCtrl.selUser, false]
                ]);
              },
              child: Text(user.name)),
        ),
        body: Column(
          children: [
            Expanded(
              child: FutureBuilder(
                future: _loadMsgFuture,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (chatCtrl.messages.isEmpty) {
                      return const Center(
                        child: Text("No Messages"),
                      );
                    } else {
                      return Obx(
                        () => ListView.builder(
                          itemCount: chatCtrl.messages.length,
                          reverse: true,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            sb.BaseMessage msg = chatCtrl.messages[index];
                            return showMessage(msg);
                          },
                        ),
                      );
                    }
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 12,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.white,
                      ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      moodIcon(),
                      Flexible(
                        child: TextFormField(
                          controller: msgCtrl,
                          keyboardType: TextInputType.emailAddress,
                          inputFormatters: [LengthLimitingTextInputFormatter(50)],
                          textInputAction: TextInputAction.send,
                          decoration: const InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.only(
                                left: 15, top: 10, bottom: 10, right: 15),
                            hintText: "Type a Message",
                            hintStyle: TextStyle(color: Color(0xFF00BFA5)),
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Enter Email";
                            }
                            return null;
                          },
                          onSaved: (value) async {
                            if (value?.isNotEmpty ?? false) {
                              await chatCtrl.addMessage(value ?? "");
                              msgCtrl.clear();
                            }
                          },
                          onFieldSubmitted: (value) async {
                            if (value.isNotEmpty) {
                              await chatCtrl.addMessage(value);
                              msgCtrl.clear();
                            }
                          },
                        ),
                      ),
                      attachFile(),
                      camera(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showMessage(sb.BaseMessage msg) {
    bool isMyMessage = msg.sender?.userId == chatCtrl.user.userId;
    final f =  DateFormat('yyyy-MM-dd hh:mm a');
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment:
          isMyMessage ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.8,
            ),
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Column(
              crossAxisAlignment: isMyMessage?CrossAxisAlignment.end:CrossAxisAlignment.start,
              children: [
                Text(
                  msg.message,
                  textAlign: isMyMessage ? TextAlign.end : TextAlign.start,
                  style: const TextStyle(
                    fontSize: 20,
                  ),
                ),
                Text(
                  f.format(DateTime.fromMillisecondsSinceEpoch(msg.createdAt)).toString(),
                  textAlign: isMyMessage ? TextAlign.end : TextAlign.start,
                  style: const TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
