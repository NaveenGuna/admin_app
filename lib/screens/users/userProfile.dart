import 'dart:io';

import 'package:accordion/controllers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:admin_app/resources/colors.dart';
import 'package:admin_app/screens/fireStore/dots.dart';
import 'package:admin_app/screens/fireStore/entity/InviteTracker.dart';
import 'package:admin_app/screens/fireStore/entity/User.dart';
import 'package:admin_app/screens/fireStore/entity/userRoles.dart';
import 'package:accordion/accordion.dart';

import '../fireStore/sendMail.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  late User user;
  bool isEdit = false;

  late InviteTracker tracker;

  late UserRoles role;

  @override
  void initState() {
    // TODO: implement initState
    try{
      user = Get.arguments[0][0];
      isEdit = Get.arguments[0][1];
      role = Get.find<UserRoles>();
      Logger().i(user.toMap());
      Logger().i(role);
    }catch(e){
      Logger().e(e);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: primaryColor,
          leading: InkWell(onTap: () => Get.back(), child: const Icon(Icons.arrow_back, color: Colors.white,)),
          title: const Text("Profile Details", style: TextStyle(color: Colors.white)),
          // centerTitle: true,
        ),
        body: FutureBuilder(
          future: getInvitationData(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.hasError) {
              return const Center(
                child: Text("Could not load"),
              );
            } else {
              return showData();
            }
          },
        ),
      ),
    );
  }

  Widget showImageAvatar() {
    if (user.imageUrl.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: CircleAvatar(
          backgroundColor: primaryColor,
          radius: MediaQuery.of(context).size.width * 0.125,
          child: Transform.scale(
            scale: 2,
            child: const Icon(
              Icons.person,
            ),
          ),
        ),
      );
    } else {
      return Container(
        padding: const EdgeInsets.all(2),
        decoration: BoxDecoration(color: primaryColor, shape: BoxShape.circle),
        child: ClipOval(
          child: SizedBox.fromSize(
            size: const Size.fromRadius(48),
            child: CachedNetworkImage(imageUrl: user.imageUrl, fit: BoxFit.cover),
          ),
        ),
      );
    }
  }

  pickFileAndUpload() async {
    final FilePickerResult? res = await FilePicker.platform.pickFiles();
    if (res != null) {
      PlatformFile file = res.files.first;
      if (file.path!.isNotEmpty) {
        FirebaseStorage.instance.ref().child(file.name).putFile(File(file.path ?? "")).then((p0) async {
          user.imageUrl = await p0.ref.getDownloadURL();
          FirebaseFirestore.instance.collection(FireStoreDots.userCollection).doc(user.documentId).update({
            "imageUrl": user.imageUrl,
          }).then((value) async {
            await ScaffoldMessenger.of(context)
                .showSnackBar(const SnackBar(
                  content: Text("Profile Updated"),
                ))
                .closed;
            Get.back();
          });
        });
      }
    }
  }

  Widget showAdminButtons() {
    if (tracker.status == 0) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.25),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ElevatedButton(
              onPressed: () {
                updateUser(1);
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.lightGreen),
              child: const Text(
                "Approve",
                style: TextStyle(color: Colors.white),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                updateUser(2);
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.redAccent),
              child: const Text(
                "Reject",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      );
    } else {
      return tracker.status == 1
          ? const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Approved",
                style: TextStyle(color: Colors.lightGreen, fontSize: 18),
              ),
            )
          : const Text(
              "Rejected",
              style: TextStyle(color: Colors.redAccent, fontSize: 18),
            );
    }
  }

  Future getInvitationData() async {
    Logger().wtf(user.toMap());
    var data = await FirebaseFirestore.instance.collection(FireStoreDots.inviteTrackerCollection).where("emailId", isEqualTo: user.emailId).limit(1).get();
    if (data.docs.isEmpty) {
      return Future.error("Not found");
    }
    tracker = InviteTracker.fromMap(data.docs[0].data());
    tracker.documentId = data.docs[0].id;
    return Future.value();
  }

  // Widget showData() {
  //   return Column(
  //     mainAxisAlignment: MainAxisAlignment.start,
  //     mainAxisSize: MainAxisSize.max,
  //     children: [
  //       Row(
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         mainAxisSize: MainAxisSize.max,
  //         children: [
  //           Container(
  //             width: MediaQuery.of(context).size.width * 0.8,
  //             margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
  //             padding: const EdgeInsets.all(20),
  //             decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(12)),
  //             child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               mainAxisSize: MainAxisSize.min,
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               children: [
  //                 Stack(
  //                   children: [
  //                     showImageAvatar(),
  //                     if (isEdit)
  //                       Positioned(
  //                         bottom: 0,
  //                         right: 0,
  //                         child: InkWell(
  //                           onTap: () => pickFileAndUpload(),
  //                           child: const CircleAvatar(
  //                             radius: 15,
  //                             backgroundColor: Colors.black87,
  //                             child: Icon(
  //                               Icons.add,
  //                               color: Colors.white,
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                   ],
  //                 ),
  //                 const SizedBox(
  //                   height: 10,
  //                 ),
  //                 Text(
  //                   user.name.capitalizeFirst ?? "",
  //                   style: const TextStyle(color: Colors.black, fontSize: 18),
  //                 ),
  //                 const SizedBox(
  //                   height: 10,
  //                 ),
  //                 Row(
  //                   mainAxisAlignment: MainAxisAlignment.center,
  //                   children: [
  //                     Icon(
  //                       Icons.email,
  //                       color: primaryColor,
  //                     ),
  //                     const SizedBox(
  //                       width: 10,
  //                     ),
  //                     Text(
  //                       user.emailId,
  //                       style: const TextStyle(color: Colors.black, fontSize: 18),
  //                     ),
  //                   ],
  //                 ),
  //               ],
  //             ),
  //           ),
  //         ],
  //       ),
  //       role == UserRoles.Admin ? showAdminButtons() : const SizedBox.shrink(),
  //     ],
  //   );
  // }

  Widget showData() {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    showImageAvatar(),
                    if (isEdit)
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: InkWell(
                          onTap: () => pickFileAndUpload(),
                          child: const CircleAvatar(
                            radius: 15,
                            backgroundColor: Colors.black87,
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                // Text(
                //   user.name.capitalizeFirst ?? "",
                //   style: const TextStyle(color: Colors.black, fontSize: 18),
                // ),
                // const SizedBox(
                //   height: 10,
                // ),
                Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: (Accordion(
                        maxOpenSections: 2,
                        disableScrolling: true,
                        headerBackgroundColorOpened: Colors.black54,
                        scaleWhenAnimating: true,
                        openAndCloseAnimation: true,
                        headerPadding:
                        const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                        sectionOpeningHapticFeedback: SectionHapticFeedback.heavy,
                        sectionClosingHapticFeedback: SectionHapticFeedback.light,
                        children: [
                          AccordionSection(
                            isOpen: false,
                            // leftIcon: const Icon(Icons.insights_rounded, color: Colors.white),
                            rightIcon: 	Icon(Icons.keyboard_arrow_down, color: Colors.black, size: 25),
                            flipRightIconIfOpen: true,
                            headerBackgroundColor: Colors.white,
                            headerPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                            // headerBackgroundColorOpened: Colors.red,
                            contentBorderColor: Colors.white,
                            header: Text('Personal Details', style: TextStyle(color: Colors.black, fontSize: 20)),
                            content: Container(child: Column(
                              children: [
                                Row(
                                  children: [
                                    Text("Name", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.businessName, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Email", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.emailId, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Mobile Number", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.phoneNo, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                              ],
                            )),
                            contentHorizontalPadding: 20,
                            contentBorderWidth: 1,
                            // onOpenSection: () => print('onOpenSection ...'),
                            // onCloseSection: () => print('onCloseSection ...'),
                          ),
                          AccordionSection(
                            isOpen: false,
                            // leftIcon: const Icon(Icons.insights_rounded, color: Colors.white),
                            rightIcon: 	Icon(Icons.keyboard_arrow_down, color: Colors.black, size: 25),
                            flipRightIconIfOpen: true,
                            headerBackgroundColor: Colors.white,
                            headerPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                            // headerBackgroundColorOpened: Colors.red,
                            contentBorderColor: Colors.white,
                            header: Text('Business Details', style: TextStyle(color: Colors.black, fontSize: 20)),
                            content: Container(child: Column(
                              children: [
                                Row(
                                  children: [
                                    Text("Business Name", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.name, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Incorporation Type", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.incorporationType, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Location", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.location, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Website", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.website, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Vision", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.vision, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Type", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.servicesOrProducts, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Key Functionary", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.keyFunctionary, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Text("Description", style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(user.description, style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                              ],
                            )),
                            contentHorizontalPadding: 20,
                            contentBorderWidth: 1,
                            // onOpenSection: () => print('onOpenSection ...'),
                            // onCloseSection: () => print('onCloseSection ...'),
                          ),

                        ],
                      )
                      ),
                    )

                    // Icon(
                    //   Icons.email,
                    //   color: primaryColor,
                    // ),
                    // const SizedBox(
                    //   width: 10,
                    // ),
                    // Text(
                    //   user.emailId,
                    //   style: const TextStyle(color: Colors.black, fontSize: 18),
                    // ),

                  ],
                ),
              ],
            ),
            role == UserRoles.Admin ? showAdminButtons() : const SizedBox.shrink(),
          ],
        ),
      ),
    );
  }

  void updateUser(int status) {
    var applicationStatus = status == 1 ? "Approved" : "Rejected";
    SendMail sendmail = SendMail();
    sendmail.sendAMailTo(user.emailId, user.businessName, applicationStatus);
    FirebaseFirestore.instance.collection(FireStoreDots.inviteTrackerCollection).doc(tracker.documentId).update({
      "status": status,
    }).then((value) async {
      await ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(
            content: status == 1 ? const Text("User Approved") : const Text("User Rejected"),
          ))
          .closed;
      Get.back();
    });
  }
}
