import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:admin_app/resources/colors.dart';
import 'package:admin_app/routers/router.dart';
import 'package:admin_app/screens/fireStore/dots.dart';
import 'package:admin_app/screens/fireStore/entity/Admin.dart';
import 'package:admin_app/screens/fireStore/entity/InviteTracker.dart';
import 'package:admin_app/screens/fireStore/entity/User.dart';
import 'package:admin_app/screens/fireStore/entity/orgTypes.dart';
import 'package:admin_app/screens/fireStore/entity/userRoles.dart';

class UserList extends StatefulWidget {
  const UserList({Key? key}) : super(key: key);

  @override
  State<UserList> createState() => _UserListState();
}

class _UserListState extends State<UserList>{
  var _future = FirebaseFirestore.instance.collection(FireStoreDots.userCollection).get();
  String mailid ="";
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              title: const Text("Users List", style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w500,),),
              automaticallyImplyLeading: false,
              backgroundColor: primaryColor,
              actions: [
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: InkWell(
                      onTap: () {
                        Get.toNamed(GetPageRouter.userProfilePageRoute, arguments: [[user, true]])!.then((value) {
                          setState(() {
                            _future = FirebaseFirestore.instance.collection(FireStoreDots.userCollection).get();
                          });
                        });
                      },
                      child: const Icon(Icons.search,size: 26, color: Colors.white,)),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 25),
                  child: InkWell(
                      onTap: () {
                        Get.toNamed(GetPageRouter.userProfilePageRoute, arguments: [[user, true]])!.then((value) {
                          setState(() {
                            _future = FirebaseFirestore.instance.collection(FireStoreDots.userCollection).get();
                          });
                        });
                      },
                      child: const Icon(Icons.settings,size: 26, color: Colors.white,)),
                ),
              ],
            ),
          ),
      body: Column(
        children: [
          showFilter(),
          Expanded(
            child: FutureBuilder(
              future: _future,
              builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData) {
                    return generateUserData(snapshot.data);
                  } else {
                    return const SizedBox.shrink();
                  }
                } else {
                  return const SizedBox.shrink();
                }
              },
            ),
          ),
        ],
      ),
    ));
  }

  late User user;

  @override
  void initState() {
    try{
      user = Get.arguments[0][0];
    }catch(e){
      print(e);
    }
    getOrgType();
    super.initState();
  }

  List<OrgType> orgTypes = [];

  Widget generateUserData(QuerySnapshot<Map<String, dynamic>>? sData) {
    if(Get.arguments[0][0] is Admin){
      Admin admin = Get.arguments[0][0];
      mailid= admin.emailId;
    }else{
      user = Get.arguments[0][0];
      mailid = user.emailId;
    }
    List<User> users = [];
    for (var element in sData!.docs) {
      User u = User.fromMap(element.data());
      u.documentId = element.id;
      if(mailid==u.emailId){
        continue;
      }
      users.add(u);
    }
    if (users.isEmpty) {
      return Center(
          child: Text(
        "No users Found",
        style: TextStyle(color: primaryColor, fontSize: 20),
      ));
    } else {
      return showList(users);
    }
  }

  Widget showList(List<User> users) {
    return ListView.builder(
      itemCount: users.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(onTap: () async{
          var data = await FirebaseFirestore.instance.collection(FireStoreDots.inviteTrackerCollection).where("emailId", isEqualTo: users[index].emailId).limit(1).get();
          if (data.docs.isEmpty) {
            return Future.error("Not found");
          }
          InviteTracker tracker = InviteTracker.fromMap(data.docs[0].data());
          if(Get.find<UserRoles>()==UserRoles.User&&tracker.status==1){
            Get.toNamed(GetPageRouter.chatPageRoute, arguments: [[users[index], false]]);
          }else{
            Get.toNamed(GetPageRouter.userProfilePageRoute, arguments: [[users[index], false]])?.then((value) {
              setState(() {
                _future = FirebaseFirestore.instance.collection(FireStoreDots.userCollection).get();
              });
            });
          }
        } ,
            child: showSingleUser(users[index]));
      },
    );
  }

  Widget showSingleUser(User user) {
    return Container(
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        margin: const EdgeInsets.symmetric(vertical: 3, horizontal: 5),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                user.imageUrl.isNotEmpty
                    ? Container(
                        padding: const EdgeInsets.all(1),
                        decoration: BoxDecoration(color: primaryColor, shape: BoxShape.circle),
                        child: ClipOval(
                          child: SizedBox.fromSize(
                            size: const Size.fromRadius(18),
                            child: CachedNetworkImage(imageUrl: user.imageUrl, fit: BoxFit.cover),
                          ),
                        ),
                      )
                    : CircleAvatar(
                        backgroundColor: primaryColor,
                        radius: 28,
                        child: Transform.scale(
                          scale: 1,
                          child: const Icon(
                            Icons.person,
                            size: 30,
                          ),
                        ),
                      ),
                const SizedBox(
                  width: 10,
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        user.businessName,
                        style: TextStyle(color: blackColor,fontWeight: FontWeight.w500, fontSize: 20),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          user.emailId,
                          style: const TextStyle(color: Colors.black54, fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8.0, bottom: 18),
              child: Text(
                user.orgType,
                style: const TextStyle(color: Colors.black54, fontSize: 14),
              ),
            ),
          ],
        ));
  }

  Widget showFilter() {
    return FutureBuilder(
      future: FirebaseFirestore.instance.collection(FireStoreDots.orgTypesCollection).get(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const SizedBox.shrink();
        } else if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            orgTypes = [];
            orgTypes.add(OrgType(0, "All"));
            for (var element in snapshot.data.docs) {
              OrgType orgType = OrgType.fromMap(element.data());
              orgTypes.add(orgType);
            }
            return Row(
              children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    "Organization Type",
                    style: TextStyle(fontSize: 16, color: primaryColor),
                  ),
                )),
                Expanded(
                  flex: 2,
                  child: DropdownButtonFormField(
                    items: orgTypes.map((OrgType value) {
                      return DropdownMenuItem<String>(
                        value: value.orgName,
                        child: Text(value.orgName),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(() {
                        if (value == "All") {
                          _future = FirebaseFirestore.instance.collection(FireStoreDots.userCollection).get();
                        } else {
                          _future = FirebaseFirestore.instance.collection(FireStoreDots.userCollection).where("orgType", isEqualTo: value).get();
                        }
                      });
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return const SizedBox.shrink();
          }
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  void getOrgType() async {
    orgTypes = [];
    orgTypes.add(OrgType(0, "All"));
    var data = await FirebaseFirestore.instance.collection(FireStoreDots.orgTypesCollection).get();
    for (var element in data.docs) {
      OrgType orgType = OrgType.fromMap(element.data());
      orgTypes.add(orgType);
    }
  }
}
