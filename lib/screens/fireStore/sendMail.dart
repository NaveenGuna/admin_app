import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:admin_app/screens/fireStore/entity/MessageMailEntity.dart';
import 'package:admin_app/screens/fireStore/entity/mailEntity.dart';

class SendMail{
  
  void sendAMailTo(String mailId,String receptientName,String orgType){
    String bodyOfMail = prepareContent(receptientName,orgType);
    MessageMailEntity msgEntity = MessageMailEntity("Your Application Status", bodyOfMail);
    MailEntity entity = MailEntity(mailId,msgEntity);
    FirebaseFirestore.instance.collection("mail").add(entity.toMap());
  }

  String prepareContent(String name,String status) {
    String body = "";
    body = "<mark>Dear $name</mark> your application has been <mark>$status</mark>";
    return body;
  }
}