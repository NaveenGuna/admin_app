import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:admin_app/main.dart';
import 'package:admin_app/resources/borderDesigns.dart';
import 'package:admin_app/resources/colors.dart';
import 'package:admin_app/resources/dimen.dart';
import 'package:admin_app/routers/router.dart';
import 'package:admin_app/screens/fireStore/dots.dart';
import 'package:admin_app/screens/fireStore/entity/Admin.dart';
import 'package:admin_app/screens/fireStore/entity/InviteTracker.dart';
import 'package:admin_app/screens/fireStore/entity/User.dart' as u;
import 'package:admin_app/screens/fireStore/entity/userRoles.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with BorderDesign, Dimension {
  double borderRadius = 4;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late String strPassword;
  late String strEmail;
  UserRoles strRole = UserRoles.User;
  RxBool showPwsd = true.obs;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.width * 0.4),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  "Admin",
                  style: TextStyle(color: blackColor, fontWeight: FontWeight.w500, fontSize: 27),
                ),
                Container(
                  // height: MediaQuery.of(context).size.height * 0.75,
                  padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 15),
                          child: showEmailTextField("Email"),
                        ),
                        Obx(() => showPasswordTextField("Password")),
                        Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 35),
                                child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        padding: EdgeInsets.symmetric(vertical: 15),
                                        backgroundColor: primaryColor
                                    ),
                                    onPressed: () {
                                      if (_formKey.currentState!.validate()) {
                                        _formKey.currentState!.save();
                                        Get.delete<UserRoles>();
                                        Get.put<UserRoles>(strRole);
                                        callAdminLoginAuth();
                                        // strRole == UserRoles.User? callUserLoginAuth():callAdminLoginAuth();
                                      }
                                    },
                                    child: const Text(
                                      "Sign in",
                                      style: TextStyle(color: Colors.white, fontSize: 22),
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget showPasswordTextField(String label) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: TextFormField(
        keyboardType: TextInputType.text,
        inputFormatters: [LengthLimitingTextInputFormatter(15)],
        obscureText: showPwsd.value,
        decoration: InputDecoration(
            isDense: true,
            contentPadding: const EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 15),
            hintText: label,
            hintStyle: const TextStyle(color: Colors.black),
            border: inputdecBorderStyle(borderRadius),
            enabledBorder: inputdecEnableborderStyle(borderRadius),
            focusedBorder: inputdecFocusedborderStyle(borderRadius),
            suffix: showPwsd.value
                ? InkWell(onTap: () => showPwsd.value = !showPwsd.value, child: const Icon(Icons.visibility))
                : InkWell(onTap: () => showPwsd.value = !showPwsd.value, child: const Icon(Icons.visibility_off))),
        validator: (value) {
          if (value!.isEmpty) {
            return "Enter Password";
          }
          return null;
        },
        onSaved: (value) {
          strPassword = value ?? "";
        },
      ),
    );
  }

  Widget showEmailTextField(String label) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        inputFormatters: [LengthLimitingTextInputFormatter(50)],
        decoration: InputDecoration(
          isDense: true,
          contentPadding: const EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 15),
          hintText: label,
          hintStyle: const TextStyle(color: Colors.black),
          border: inputdecBorderStyle(borderRadius),
          enabledBorder: inputdecEnableborderStyle(borderRadius),
          focusedBorder: inputdecFocusedborderStyle(borderRadius),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return "Enter Email";
          }
          return null;
        },
        onSaved: (value) {
          strEmail = value ?? "";
        },
      ),
    );
  }

  // void callAdminLoginAuth() {
  //   var data = FirebaseFirestore.instance.collection(FireStoreDots.adminCollection).where("emailId", isEqualTo: strEmail).limit(1).get();
  //   print(data);
  //   Admin admin;
  //   data.then((value) {
  //     if (value.docs.isNotEmpty) {
  //       admin = Admin.fromMap(value.docs[0].data());
  //       if (admin.password == strPassword) {
  //         // admin.documentId = value.docs[0].id;
  //         Get.delete<UserRoles>();
  //         Get.put<UserRoles>(UserRoles.Admin);
  //         Get.toNamed(GetPageRouter.userListRoute,arguments: [[admin],false]);
  //       } else {
  //         ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
  //           content: Text("Password wrong"),
  //         ));
  //       }
  //     } else {
  //       ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
  //         content: Text("Mail ID not found"),
  //       ));
  //     }
  //   }).onError((error, stackTrace) {
  //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //       content: Text(error.toString()),
  //     ));
  //   });
  // }

  void callAdminLoginAuth() async {

    var querySnapshot = await FirebaseFirestore.instance.collection(FireStoreDots.adminCollection)
        .where("emailId", isEqualTo: strEmail)
        .limit(1)
        .get();

    if (querySnapshot.docs.isNotEmpty) {
      var admin = Admin.fromMap(querySnapshot.docs[0].data());
      if (admin.password == strPassword) {
        // admin.documentId = querySnapshot.docs[0].id;
        Get.delete<UserRoles>();
        Get.put<UserRoles>(UserRoles.Admin);
        Get.toNamed(GetPageRouter.userListRoute, arguments: [[admin], false]);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Password wrong"),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Mail ID not found"),
      ));
    }
  }


  Widget showEmailDialog() {
    final GlobalKey<FormFieldState> _emailKey = GlobalKey<FormFieldState>();
    return AlertDialog(
      title: const Text("Enter mailId"),
      content: TextFormField(
        key: _emailKey,
        keyboardType: TextInputType.emailAddress,
        inputFormatters: [LengthLimitingTextInputFormatter(50)],
        decoration: InputDecoration(
          isDense: true,
          contentPadding: const EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 15),
          hintText: "Email Id",
          hintStyle: const TextStyle(color: Colors.black),
          border: inputdecBorderStyle(borderRadius),
          enabledBorder: inputdecEnableborderStyle(borderRadius),
          focusedBorder: inputdecFocusedborderStyle(borderRadius),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return "Enter Email";
          }
          return null;
        },
        onSaved: (value) {
          strEmail = value ?? "";
        },
      ),
      actions: [
        ElevatedButton(
            onPressed: () {
              if (_emailKey.currentState!.validate()) {
                _emailKey.currentState!.save();

              }
            },
            child: const Text(
              "Continue",
              style: TextStyle(color: Colors.white, fontSize: 22),
            ))
      ],
    );
  }

}
